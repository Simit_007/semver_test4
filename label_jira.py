# This code sample uses the 'requests' library:
# http://docs.python-requests.org
import requests
from requests.auth import HTTPBasicAuth
import json

url = "https://onestopml.atlassian.net/rest/api/3/issue/PROJ-1"

auth = HTTPBasicAuth("simittomar8@gmail.com", "clhnCQAnZMbAUg1AfeMA640E")

headers = {
   "Accept": "application/json",
   "Content-Type": "application/json"
}

payload = json.dumps( {
  "update": {
    "labels": [
      {
        "add": "triaged"
      }
    ]
  }
} )

response = requests.request(
   "PUT",
   url,
   data=payload,
   headers=headers,
   auth=auth
)
print(response)

# print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))