
import json
import os
import sys
import semver
import subprocess
import requests
import re
import traceback

f = open('mr_commits.json')
mr_commits = json.load(f)


def get_jira_ids_list(mr_commits):

    try:
        if mr_commits:
            jira_ids_list = []
            for mr_commit in mr_commits:
                jira_id = extract_jira_id(mr_commit['message'])
                if jira_id:
                    if jira_id not in jira_ids_list:
                        jira_ids_list.append(jira_id)
            return jira_ids_list
        else:
            print('Skipping extraction of Jira Ids as no commits found for the merge request')
    except KeyError as ke:
        print(f"Oops !! key {ke} is missing in the commit message body {mr_commit}")
        traceback.print_exc()
    except Exception as e:
        print('Oops !! an exception occurred while getting the list of all jira ids from the commits messages')
        traceback.print_exc()

def extract_jira_id(message):
    try:
        found = re.search("[A-z]+-[0-9]+", message).group(0)
        return found
    except IndexError as ie:
        print(ie)
    except Exception as e:
        print('Oops !! an exception occured while extracting the jira id from a commit message')
        traceback.print_exc()


jira_ids_list = get_jira_ids_list(mr_commits)
print('jira_ids_list', jira_ids_list)